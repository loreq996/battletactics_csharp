﻿using System;

namespace CsharpTasks_Vincenzo_Daidone
{
    class Program
    {
        static void Main(string[] args)
        {
            /*I decided to implement the side of reading and saving 
             * files because it was the part with graphic objects less involved.
             * In fact this programs reflects the saving and reading of both a map file and a character file
             */

            ReadSaveMapclass rdsv = new ReadSaveMapclass(Environment.CurrentDirectory + @"\resources\MappaConTrincea.txt");
            rdsv.ReadFile();

            var Map =  rdsv.getMap();

            Console.WriteLine("Contents of the maps:");
            string text = System.IO.File.ReadAllText(Environment.CurrentDirectory + @"\resources\MappaConTrincea.txt");
            System.Console.WriteLine(text);

            Console.WriteLine("The map readed is the below: ");
            foreach(var Element in Map)
            {
                Console.WriteLine(Element);
            }

            ReadSaveCharClass rsCharclass = new ReadSaveCharClass(Environment.CurrentDirectory + @"\resources\Eadgar.txt");
            rsCharclass.ReadFile();

            Console.WriteLine();
            Console.WriteLine("The character readed is below: ");
            Console.WriteLine(rsCharclass.CharacterToString());
        }
    }
}
