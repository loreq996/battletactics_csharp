﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace CsharpTasks_Vincenzo_Daidone
{
    class ReadSaveMapclass
    {
        string file;

        Dictionary<Point, string> Map = new Dictionary<Point, string>();

        public ReadSaveMapclass(string filepath)
        {
            bool dirExist = File.Exists(filepath);

            if (dirExist)
            {
                this.file = filepath;
            }
            else
            {
                Console.WriteLine("File does not exist");
            }
        }

        public String ReadFile()
        {
            if(file == "" || file == null)
            {
                return "File does not exist";
            }
            else
            {
                this.ReadAndDivide();
            }
            return "Operation failed";
        }

        private void ReadAndDivide()
        {
            int columnsNo = 0;
            int rowsNo = 0;

            foreach (string line in System.IO.File.ReadAllLines(file))
            {
                var characters= line.Split();

                foreach(string character in characters)
                {
                    Map.Add(new Point(rowsNo, columnsNo), character);
                    columnsNo++;
                }
                rowsNo++;
                columnsNo = 0;
            }
        }

        public Dictionary<Point,string> getMap()
        {
            return this.Map;
        }

    }
}
