﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace CsharpTasks_Vincenzo_Daidone
{
    class ReadSaveCharClass
    {
        string file;

        string name;
        string strenght;
        string defense;
        string hp;

        MovementType mt;
        AttackType at;

        const int NUM_STAT = 6;

        enum MovementType
        {
            NONE = 0,
            FEET = 1,
            FLYING = 2
        }
        enum AttackType
        {
            NONE = 0,
            SWORD = 1,
            POLEARM = 2,
            BOW = 3
        }

        public ReadSaveCharClass(string filepath)
        {
            bool dirExist = File.Exists(filepath);

            if (dirExist)
            {
                this.file = filepath;
            }
            else
            {
                Console.WriteLine("File does not exist");
            }
        }

        public String ReadFile()
        {
            if (file == "" || file == null)
            {
                return "File does not exist";
            }
            else
            {
                this.ReadAndAnalyze();
            }
            return "Operation failed";
        }

        private void ReadAndAnalyze()
        {
            int first = 0;

            string[] stats = new string[NUM_STAT];
            int i = 0;

            foreach (string line in System.IO.File.ReadAllLines(file))
            {
                var characters = line.Split();

                stats[i] = characters[1];
                i++;
            }

            this.name = stats[0];
            this.strenght = stats[1];
            this.hp = stats[2];
            this.defense = stats[3];

            this.mt = (MovementType) Convert.ToInt32(stats[4]);
            this.at = (AttackType)Convert.ToInt32(stats[5]);

        }

        public string CharacterToString()
        {
            return "Name: " + this.name + "\n" + 
                    "Forza: " + this.strenght + "\n" + 
                        "Salute: " + this.hp + "\n" + 
                            "Difesa: " + this.defense + "\n" + 
                                "MovementType: " + this.mt + "\n" + 
                                    "AttackType: " + this.at + "\n";
        }
    }
}
