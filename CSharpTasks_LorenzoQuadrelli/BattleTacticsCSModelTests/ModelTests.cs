using Microsoft.VisualStudio.TestTools.UnitTesting;
using BattleTactics_LorenzoQuadrelli.src.model.entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleTacticsCSModelTests
{
    [TestClass]
    public class ModelTests
    {
        [TestMethod]
        public void Test()
        {
            var heroMario = new Hero("Mario", 20, 18, 3, MovementType.FEET, AttackType.SWORD, new Tuple<int, int>(0, 0));
            var heroLuigi = new Hero("Luigi", 40, 19, 7, MovementType.FLYING, AttackType.BOW, new Tuple<int, int>(0, 1));
            var villainMal = new Hero("Mal", 10, 2, 1, MovementType.FLYING, AttackType.POLEARM, new Tuple<int, int>(1, 1));
            var obstacle = new Obstacle("obstacle", new Tuple<int, int>(1, 0));

            var heroes = new List<Hero> { heroMario, heroLuigi };
            var villains = new List<Hero> { villainMal };
            var obstacles = new List<Obstacle> { obstacle };

            //Test that obstacles always receive a single point of damage
            Assert.AreEqual(3, obstacle.CurrentHP);
            obstacle.LoseHP(heroMario.Attack);
            heroMario.AttackStatus = AttackStatus.EXHAUSTED;
            Assert.AreEqual(2, obstacle.CurrentHP);

            //Test that energizeAll energizes all exhausted heroes of a team
            Teams.EnergizeAllTeam(heroes);
            Assert.IsTrue(heroes.Where(h => h.AttackStatus == AttackStatus.EXHAUSTED).Count() == 0);

            //Test that villainMal gets killed after its HP reaches 0
            foreach(var hero in heroes)
            {
                villainMal.LoseHP(hero.Attack);
            }
            Assert.IsTrue(Teams.IsTeamDead(villains));
        }
    }
}
