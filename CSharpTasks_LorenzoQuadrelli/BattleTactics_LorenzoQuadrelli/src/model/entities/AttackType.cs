﻿namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    public enum AttackType
    {
        NONE = 0, SWORD = 1, POLEARM = 2, BOW = 3
    }
}

