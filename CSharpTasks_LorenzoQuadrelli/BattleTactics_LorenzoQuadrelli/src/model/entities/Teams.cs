﻿using System.Collections.Generic;
using System.Linq;

namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    /**
     * 
     * Functional class for team related stuff.
     *
     */
    public static class Teams
    {
        private static List<Hero> GetAliveMembersInTeam(List<Hero> team) => team.Where(h => h.Status.Equals(EntityStatus.ALIVE)).ToList();


        public static bool IsTeamExhausted(List<Hero> team) => GetAliveMembersInTeam(team)
                                                                .Where(h => h.AttackStatus.Equals(AttackStatus.EXHAUSTED) && h.MovementStatus.Equals(MovementStatus.EXHAUSTED))
                                                                .Count() == GetAliveMembersInTeam(team).Count;


        public static bool IsTeamDead(List<Hero> team) => GetAliveMembersInTeam(team).Count == 0;

        public static void EnergizeAllTeam(List<Hero> team) => GetAliveMembersInTeam(team).ForEach(h => h.Energize());

    }
}