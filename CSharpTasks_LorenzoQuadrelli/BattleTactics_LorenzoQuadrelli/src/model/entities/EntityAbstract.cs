﻿using System;
using System.Collections.Generic;

namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    public abstract class EntityAbstract : IEntity
    {
        private Tuple<int, int> _position;
        private int _currentHP;

        public string Name { get; }
        public int MaxHP { get; }

        public int CurrentHP => _currentHP;
        public int Attack { get; }

        public int Defense { get; }

        public Tuple<int, int> Position => _position;
        public EntityType Type { get; }

        public EntityStatus Status { get; set; }

        public MovementType MovementType { get; }

        public AttackType AttackType { get; }

        public AttackStatus AttackStatus { get; set; }

        public MovementStatus MovementStatus { get; set; }

        protected internal EntityAbstract(string name, int maxHP, int attack, int defense, EntityType entityType, EntityStatus entityStatus, MovementType movementType, AttackType attackType, Tuple<int, int> initialPosition)
        {
            this.Name = name;
            this.MaxHP = maxHP;
            this._currentHP = maxHP;
            this.Attack = attack;
            this.Defense = defense;
            this.Type = entityType;
            this.Status = entityStatus;
            this.MovementType = movementType;
            this.AttackType = attackType;
            this._position = new Tuple<int, int>(initialPosition.Item1, initialPosition.Item2);
        }

        protected internal abstract int CalculateDamage(int incomingDamage);

        public void Energize()
        {
            this.AttackStatus = AttackStatus.AVAILABLE;
            this.MovementStatus = MovementStatus.AVAILABLE;
        }

        public List<string> GetInfo() => new List<string> { this.Name, this.MaxHP.ToString(), this.CurrentHP.ToString(),
                        this.Attack.ToString(), this.Defense.ToString(), this.Type.ToString(),
                        this.Status.ToString(), this.MovementType.ToString(), this.AttackType.ToString(),
                        this.AttackStatus.ToString(), this.MovementStatus.ToString(), this.Position.ToString() };

        public bool IsAttackExhausted() => this.AttackStatus.Equals(AttackStatus.EXHAUSTED);

        public bool IsMovementExhausted() => this.MovementStatus.Equals(MovementStatus.EXHAUSTED);

        public void LoseHP(int incomingDamage)
        {
            this._currentHP -= this.CalculateDamage(incomingDamage);
            if (this.CurrentHP <= 0)
            {
                this.Status = EntityStatus.DEAD;
            }
        }

        public void Move(Tuple<int, int> newPosition)
        {
            if (this.MovementType != MovementType.NONE)
            {
                this._position = new Tuple<int, int>(newPosition.Item1, newPosition.Item2);
            }
        }
    }
}
