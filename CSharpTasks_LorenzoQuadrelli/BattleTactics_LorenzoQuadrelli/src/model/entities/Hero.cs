﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    public class Hero : EntityAbstract
    {
        public Hero(string name, int maxHP, int attack, int defense,
                  MovementType movementType, AttackType attackType,
                  Tuple<int, int> initialPosition) : this(name, maxHP, attack, defense, EntityType.HERO, EntityStatus.ALIVE, movementType, attackType,
                    initialPosition)
        {
        }

        private Hero(string name, int maxHP, int attack, int defense, EntityType entityType,
                 EntityStatus entityStatus, MovementType movementType, AttackType attackType,
                 Tuple<int, int> initialPosition) : base(name, maxHP, attack, defense, entityType, entityStatus, movementType, attackType, initialPosition)
        {
        }
        protected internal override int CalculateDamage(int incomingDamage) => incomingDamage - this.Defense;
    }
}
