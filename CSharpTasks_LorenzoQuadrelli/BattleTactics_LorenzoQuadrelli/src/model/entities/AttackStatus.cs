﻿namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    public enum AttackStatus
    {
        AVAILABLE, EXHAUSTED
    }
}

