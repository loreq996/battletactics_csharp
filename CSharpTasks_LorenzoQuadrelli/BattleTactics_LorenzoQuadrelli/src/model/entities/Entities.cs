﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    public static class Entities
    {
        public static List<AttackType> GetAllAttackTypes() => new List<AttackType> { AttackType.SWORD, AttackType.POLEARM, AttackType.BOW, AttackType.NONE };
        public static List<MovementType> GetAllMovementTypes() => new List<MovementType> { MovementType.FEET, MovementType.FLYING, MovementType.NONE };
    }
}
