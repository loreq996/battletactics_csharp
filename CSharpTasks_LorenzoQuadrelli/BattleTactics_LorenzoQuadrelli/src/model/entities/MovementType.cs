﻿namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    public enum MovementType
    {
        NONE, FLYING = 2, FEET = 4
    }
}

