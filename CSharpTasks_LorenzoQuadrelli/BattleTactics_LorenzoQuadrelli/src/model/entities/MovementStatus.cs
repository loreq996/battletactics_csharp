﻿namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    public enum MovementStatus
    {
        AVAILABLE, EXHAUSTED
    }
}

