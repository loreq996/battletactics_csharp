﻿using System;

namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    public class Obstacle : EntityAbstract
    {
        public Obstacle(string name, Tuple<int, int> initialPosition) : this(name, 3, 0, 0, EntityType.OBSTACLE, EntityStatus.ALIVE, MovementType.NONE, AttackType.NONE,
                    initialPosition)
        {
        }

        private Obstacle(string name, int maxHP, int attack, int defense, EntityType entityType,
                 EntityStatus entityStatus, MovementType movementType, AttackType attackType,
                 Tuple<int, int> initialPosition) : base(name, maxHP, attack, defense, entityType, entityStatus, movementType, attackType, initialPosition)
        {
        }
        protected internal override int CalculateDamage(int incomingDamage) => 1;
    }
}
