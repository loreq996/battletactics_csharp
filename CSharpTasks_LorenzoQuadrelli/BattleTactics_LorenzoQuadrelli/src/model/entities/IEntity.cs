﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleTactics_LorenzoQuadrelli.src.model.entities
{
    interface IEntity
    {
        string Name { get; }
        int MaxHP { get; }
        int CurrentHP { get; }
        int Attack { get; }
        int Defense { get; }
        Tuple<int, int> Position { get; }
        EntityType Type { get; }
        EntityStatus Status { get; set; }
        MovementType MovementType { get; }
        AttackType AttackType { get; }
        AttackStatus AttackStatus { get; set; }
        MovementStatus MovementStatus { get; set; }

        void Move(Tuple<int, int> newPosition);
        List<string> GetInfo();
        void LoseHP(int incomingDamage);
        bool IsMovementExhausted();
        bool IsAttackExhausted();
        void Energize();
    }
}
