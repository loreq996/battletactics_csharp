﻿using System;
using System.Collections.Generic;

namespace CTasks_SamueleBernardi
{
    public class ChooseArenaMap
    {
        public ChooseArenaMap(string file)
        {
            int width = 0;
            int height = 0;
            int cells = 0;
            string[] charArray = null;
            List<char> arenaMap = new List<char>();

            foreach (string line in System.IO.File.ReadAllLines(file))
            {
                charArray = line.Split();

                foreach (string character in charArray)
                {
                    width++;
                    cells++;
                }
                height++;
            }
            width /= height;

            for (int i = 0; i < cells; i++)
            {
                if ("S" == (charArray[i]))
                {
                    arenaMap.Add('S');
                }
                if ("E" == (charArray[i]))
                {
                    arenaMap.Add('E');
                }
                if ("M" == (charArray[i]))
                {
                    arenaMap.Add('M');
                }
                if ("W" == (charArray[i]))
                {
                    arenaMap.Add('W');
                }
                if ("O" == (charArray[i]))
                {
                    arenaMap.Add('O');
                }
                if ("T" == (charArray[i]))
                {
                    arenaMap.Add('S');
                }
            }
        }
    }
}
