﻿using System;
using System.IO;
using System.Collections.Generic;
namespace CTasks_SamueleBernardi
{
    public class LoadHeroImages
    {
        enum MovementType
        {
            NONE = 0,
            FEET = 1,
            FLYING = 2
        }
        enum AttackType
        {
            NONE = 0,
            SWORD = 1,
            POLEARM = 2,
            BOW = 3
        }
        class Hero
        {
            string name;
            string strenght;
            string defense;
            string hp;
            MovementType mt;
            AttackType at;

            public string Name { get => name; set => name = value; }
            public string Strenght { get => strenght; set => strenght = value; }
            public string Defense { get => defense; set => defense = value; }
            public string Hp { get => hp; set => hp = value; }
            public MovementType Mt { get => mt; set => mt = value; }
            public AttackType At { get => at; set => at = value; }
        }


        public LoadHeroImages(string directory)
        {
            string[] filePaths = Directory.GetFiles(directory);
            int HeroAttributes = 6;
            List<Hero> HeroList = new List<Hero>();
            Hero HeroDummy = new Hero();

            string[] stats = new string[HeroAttributes];

            for (int i = 0; i < filePaths.Length; i++)
            {
                int j = 0;
                foreach (string line in System.IO.File.ReadAllLines(filePaths[i]))
                {
                    var characters = line.Split();

                    stats[j] = characters[1];
                    j++;
                }

                HeroDummy.Name = stats[0];
                HeroDummy.Strenght = stats[1];
                HeroDummy.Hp= stats[2];
                HeroDummy.Defense = stats[3];

                HeroDummy.Mt = (MovementType)Convert.ToInt32(stats[4]);
                HeroDummy.At = (AttackType)Convert.ToInt32(stats[5]);
                HeroList.Add(HeroDummy);
            }

        }
    }
}