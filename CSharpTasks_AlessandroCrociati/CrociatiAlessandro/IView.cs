﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrociatiAlessandro
{
    interface IView
    {
        void StartView();
        void ShowGameScreen(KeyValuePair<int, int> p);

        void ShowSelectionInfo(BattleController bc,string maxHp, string name, string attack, String def);

        void UpdateEntityPosition(BattleController bc, KeyValuePair<int, int> newPos, KeyValuePair<int, int> oldPos);

        void ShowVictoryMessage(BattleController bc, string victoryMessage);

        void UpdateGameTurn(BattleController bc, string turn);
    }
}
