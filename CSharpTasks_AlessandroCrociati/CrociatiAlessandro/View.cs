﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrociatiAlessandro
{
    class View : IView
    {
        public void ShowGameScreen(KeyValuePair<int, int> p)
        {
            try
            {
                BattleScene bs = new BattleScene(new BattleController(), p);
            }
            catch(IOException e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }
        public void ShowSelectionInfo(BattleController bc,string maxHp, string name, string attack, string defense)
        {
            bc.ShowInfo(maxHp, name, attack, defense);
        }

        public void ShowVictoryMessage(BattleController bc, string victoryMessage)
        {
            bc.VictoryMessage(victoryMessage);
        }

        public void StartView()
        {
             //Application.launch(MainWindow.class);
        }

        public void UpdateEntityPosition(BattleController bc, KeyValuePair<int, int> newPos, KeyValuePair<int, int> oldPos)
        {
            bc.UpdatePosition(newPos, oldPos);
        }

        public void UpdateGameTurn(BattleController bc, string turn)
        {
            bc.UpdateTurn(turn);
        }
    }
}
