﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CrociatiAlessandro
{
    class HeroButton:Button
    {
        private int identifier;
        public int getIdentifier()
        {
            return identifier;
        }
        public void setIdentifier(int newIdentifier)
        {
            identifier = newIdentifier;
        }
    }
}
