﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CrociatiAlessandro
{
   public sealed class BattleController
    {
        private Grid grid;
        private Label srcMaxHp;
        private Label srcName;
        private Label srcAttack;
        private Label srcDef;
        private Label playerTurn;
        private Label winner;
        public void InitData(BattleController bc, KeyValuePair<int, int> p)
        {
            grid.ColumnDefinitions.Clear();
            grid.RowDefinitions.Clear();
            for (int i = 0; i < p.Key; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition());
                for (int j = 0; j < p.Value; j++)
                {
                    grid.ColumnDefinitions.Add(new ColumnDefinition());
                    Button btn = new Button();
                    btn.Click += (s, e) =>
                    {
                        this.CellClick(s,e);
                    };

                    if (i == 0)
                    {
                        grid.ColumnDefinitions.Add(new ColumnDefinition());
                    }
                    grid.Children.Add(btn);
                }
            }       
        }

        internal void ShowInfo()
        {
            throw new NotImplementedException();
        }

        private void CellClick(object sender, EventArgs e)
        {
            Button btnSource = (Button)sender;
        }

        private void CellHover(Button b)
        {
            bool focus = b.IsFocused;
            if(focus == true)
            {
                /*ControllerImpl.getInstance().battleCursorInfo(this.bc);
                b.setStyle("-fx-border-color: #ff0000; -fx-border-width: 1px;");*/
            }
            else
            {
                //b.setStyle("-fx-border-color: transparent;");
            }
        }

        public void ShowInfo(string maxHp, string name, string atk, string def)
        {
            srcMaxHp.Content = maxHp;
            srcName.Content = name;
            srcAttack.Content = atk;
            srcDef.Content = def;
        }

       public void Quit()
        {
            Environment.Exit(1);
        }

        public void UpdateTurn(String turn)
        {
            this.playerTurn.Content = turn;
        }

        public void SelectionMovementCandidates(List<KeyValuePair<int, int>> positions)
        {
            if (positions.Any())
            {
                this.ResetSelection();
            }

            foreach (KeyValuePair<int, int> p in positions)
            {
                Button b = GetButton(grid, p.Value, p.Key);
            }
        }

        private void ResetSelection()
        {
            foreach (Button b in grid.Children)
            {
                //b.setStyle(null);
            }
        }
        private Button GetButton (Grid grid, int col, int row)
        {
            foreach (Button b in grid.Children)
            {
                if(Grid.GetColumn(b) == col && Grid.GetRow(b) == row)
                {
                    return b;
                }  
            }
            return null;
        }
        public void UpdatePosition( KeyValuePair<int, int> newPos, KeyValuePair<int, int> oldPos)
        {
             Button newBtn = GetButton(grid, newPos.Value, newPos.Key);
             Button oldBtn = GetButton(grid, oldPos.Value, oldPos.Key);
        }
        public void VictoryMessage(String victoryMessage)
        {
            this.winner.Content = victoryMessage;
        }
    }
}
